package com.example.countries.android_platform.network

import android.annotation.TargetApi
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.example.countries.android_platform.extensions.connectivityManager
import com.example.countries.data.source.INetworkChecker
import com.example.countries.presentation.extensions.networkInfo

class NetworkChecker constructor(private val context: Context): INetworkChecker {

  override fun isConnected() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
    postMarshmallowInternetCheck(context.connectivityManager)
  } else {
    preMarshmallowInternetCheck(context.connectivityManager)
  }

  private fun preMarshmallowInternetCheck(connectivityManager: ConnectivityManager?): Boolean {
    val activeNetwork = connectivityManager?.activeNetworkInfo
    if (activeNetwork != null) {
      return (activeNetwork.type == ConnectivityManager.TYPE_WIFI ||
              activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
    }
    return false
  }

  @TargetApi(Build.VERSION_CODES.M)
  private fun postMarshmallowInternetCheck(connectivityManager: ConnectivityManager?): Boolean {
    val connection = connectivityManager?.getNetworkCapabilities(connectivityManager.activeNetwork)
    return isNetworkAvailable(connection)
  }

  private fun isNetworkAvailable(connection: NetworkCapabilities?) =
    connection != null && (connection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
            connection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
}
