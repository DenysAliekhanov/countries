package com.example.countries.android_platform.data_base.countries

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.countries.data.dto.FavoriteCountry
import com.example.countries.domain.entities.IFavoriteCountry

@Entity(tableName = "countries", indices = [Index(value = ["id"], unique = true)])
data class CountryEntity(
  @PrimaryKey val id: String,
  @ColumnInfo(name = "name") val name: String
) {

  companion object {
    fun fromLocalCountryEntity(localCountryEntity: IFavoriteCountry): CountryEntity =
      CountryEntity(
        localCountryEntity.id, localCountryEntity.name
      )
  }

  fun toLocalCountryEntity() = FavoriteCountry(id, name)
}
