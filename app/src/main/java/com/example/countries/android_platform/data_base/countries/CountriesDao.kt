package com.example.countries.android_platform.data_base.countries

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CountriesDao {

  @Query("SELECT * FROM countries")
  fun selectAll(): List<CountryEntity>

  @Query("SELECT * FROM countries WHERE name = :name")
  fun getByName(name: String): CountryEntity

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(userEntity: CountryEntity)

  @Delete
  fun delete(userEntity: CountryEntity)
}
