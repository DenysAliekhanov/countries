package com.example.countries.android_platform.data_base

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.countries.android_platform.data_base.countries.CountriesDao
import com.example.countries.android_platform.data_base.countries.CountryEntity

private const val DB_NAME = "countries.db"

@Database(
    entities = [CountryEntity::class],
    version = 1,
    exportSchema = false
)
abstract class CountriesDatabase : RoomDatabase() {

  abstract fun countriesDao(): CountriesDao

  companion object {
    private var INSTANCE: CountriesDatabase? = null

    fun getInstance(context: Context): CountriesDatabase? {
      if (INSTANCE == null) {
        synchronized(CountriesDatabase::class) {
          INSTANCE = Room.databaseBuilder(
              context.applicationContext,
              CountriesDatabase::class.java,
              DB_NAME
          )
              .build()
        }
      }
      return INSTANCE
    }

    fun destroyInstance() {
      INSTANCE = null
    }
  }
}
