package com.example.countries.app.koin

import com.example.countries.android_platform.network.NetworkChecker
import com.example.countries.data.source.INetworkChecker
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val androidPlatformModule = module {

    single<INetworkChecker> { NetworkChecker(androidApplication().applicationContext) }
}