package com.example.countries.app

import android.app.Activity
import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import com.example.countries.app.koin.*
import com.example.countries.presentation.core.IAppLifecycle
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class CountriesApp : Application(), IAppLifecycle {
    override var foregroundActivity: AppCompatActivity? = null
        private set

    override fun onCreate() {
        super.onCreate()
        initKoinModules()
        initAppLifecycle()
    }

    private fun initKoinModules() {
        startKoin {
            // Android context
            androidContext(this@CountriesApp)
            // modules
            modules(
                listOf(
                    module { single<IAppLifecycle> { this@CountriesApp } },
                    presentationModule,
                    domainModule,
                    dataModule,
                    androidPlatformModule,
                    loggingModule
                )
            )
        }
    }

    private fun initAppLifecycle() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacklImpl() {
            override fun onActivityResumed(activity: Activity?) {
                activity?.let {
                    if (it is AppCompatActivity) {
                        foregroundActivity = it
                    } else {
                        throw RuntimeException("$it must inherit AppCompatActivity")
                    }
                }
            }
        })
    }

}
