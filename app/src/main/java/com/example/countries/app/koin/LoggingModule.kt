package com.example.countries.app.koin

import com.example.countries.BuildConfig
import com.example.countries.domain_base.logger.ILogger
import com.example.countries.presentation_base.Logger
import com.example.countries.presentation_base.ReportingTree
import org.koin.dsl.module
import timber.log.Timber

val loggingModule = module(createdAtStart = true) {
    val timberTrees = if(BuildConfig.DEBUG) {
        listOf(Timber.DebugTree(), ReportingTree())
    } else {
        listOf(ReportingTree())
    }
    single<ILogger> { Logger(timberTrees) }
}
