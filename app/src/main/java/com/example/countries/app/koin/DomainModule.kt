package com.example.countries.app.koin

import com.example.countries.domain.usecases.countries.*
import org.koin.dsl.module

val domainModule = module{

    single { GetAllFavoriteCountriesUseCase(get()) }

    single { RetrieveCountriesUseCase(get(), get()) }

    single { ChangeFavoriteCountryUseCase(get(), get(), get()) }

    single { GetFavoriteCountryByNameUseCase(get()) }

    single { CreateFavoriteCountryUseCase(get(), get()) }

    single { DeleteFavoriteCountryUseCase(get()) }

    single { SaveClickedCountryUseCase(get()) }

    single { GetClickedCountryUseCase(get()) }

}