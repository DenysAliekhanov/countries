package com.example.countries.app.koin

import com.example.countries.android_platform.data_base.CountriesDatabase
import com.example.countries.data.api.Api
import com.example.countries.data.api.AppInterceptor
import com.example.countries.data.api.Networking
import com.example.countries.data.repository.LocalRepository
import com.example.countries.data.repository.NetworkRepository
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain.repositories.INetworkRepository
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://restcountries-v1.p.rapidapi.com/"

val dataModule = module {

    single { Networking(get(), get()) }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .addNetworkInterceptor(AppInterceptor())
            .build()
    }

    single<Api> {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        retrofit.create(Api::class.java)
    }

    single<INetworkRepository> { NetworkRepository(get(), get()) }

    single<ILocalRepository> { LocalRepository(CountriesDatabase.getInstance(androidApplication().applicationContext)) }
}
