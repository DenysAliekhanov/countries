package com.example.countries.app.koin

import com.example.countries.presentation.core.MainFailureHandler
import com.example.countries.presentation.feature.details.DetailsViewModel
import com.example.countries.presentation.feature.main.MainViewModel
import com.example.countries.presentation_base.IFailureDelegate
import kotlinx.coroutines.newSingleThreadContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import kotlin.coroutines.CoroutineContext

val presentationModule = module {

    single<CoroutineContext> { newSingleThreadContext("Single Thread") }

    single<IFailureDelegate> { MainFailureHandler(get(), get()) }

    viewModel {
        MainViewModel(
            appContext = get(),
            baseContext = get(),
            failureDelegate = get(),
            retrieveCountriesUseCase = get(),
            changeFavoriteCountryUseCase = get(),
            saveClickedCountryUseCase = get()
        )
    }

    viewModel {
        DetailsViewModel(
            appContext = get(),
            baseContext = get(),
            failureDelegate = get(),
            getClickedCountryUseCase = get()
        )
    }

}
