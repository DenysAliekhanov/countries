package com.example.countries.presentation_base

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

/**
 * A [Timber.Tree] implementation that uploads all log messages of severity `WARN` or higher to Crashlytics.
 * Suitable for use in release builds since it ignores all log messages that are less severe than `WARN`.
 *
 * Requires you to have set up Crashlytics (for example in `Application` sub-class)
 */
class ReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when (priority) {
            Log.ERROR, Log.WARN -> {
                FirebaseCrashlytics.getInstance().log(message)
                if (t != null) FirebaseCrashlytics.getInstance().recordException(t)
            }
            else -> return
        }
    }

    override fun isLoggable(tag: String?, priority: Int): Boolean {
        return priority >= Log.WARN
    }
}