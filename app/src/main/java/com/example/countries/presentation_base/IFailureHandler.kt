package com.example.countries.presentation_base

import com.example.countries.domain_base.error.IFailure
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.SendChannel

typealias RetryAction = () -> Unit
typealias ReInitAction = () -> Unit

interface IFailureDelegate {
    val failureChannel: SendChannel<FailureObject>
}

data class FailureObject(
    val failure: IFailure,
    val reInit: ReInitAction,
    val retryAction: RetryAction?,
    val retryActionScope: Job
) {

    // use only IFailure to compare Failures objects
    override fun equals(other: Any?): Boolean {
        return failure == (other as FailureObject).failure
    }

    override fun hashCode(): Int {
        return failure.hashCode()
    }

    override fun toString(): String {
        return "Failure : ${failure.errorMessage}"
    }

    fun isRetryAvailable() = retryAction != null && retryActionScope.isActive

}