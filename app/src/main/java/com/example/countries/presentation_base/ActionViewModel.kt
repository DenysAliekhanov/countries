package com.example.countries.presentation_base

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.FlowUseCase
import com.example.countries.domain_base.interactor.UseCase
import com.example.countries.domain_base.logger.ILogger
import com.hunter.library.debug.HunterDebug
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import kotlin.coroutines.CoroutineContext


typealias ActionProcessor<A> = (action: A) -> Unit


abstract class ActionViewModel<I, S : I, A>(
    private val failureDelegate: IFailureDelegate,
    baseContext: CoroutineContext,
    protected var state: S,
    private val debounceActions: Long = 0L,
    private val debounceChanges: Long = 0L
) :
    ViewModel(),
    CoroutineScope,
    KoinComponent {

    protected val TAG = this::class.java.simpleName

    @Suppress("SimplifyBooleanWithConstants")
    private val DBG = false

    private val superJob = SupervisorJob()
    private lateinit var actions: Channel<A>
    private lateinit var changes: Channel<S>
    private val _uiState = object : MutableLiveData<I>() {

        override fun onActive() {
            super.onActive()
            this@ActionViewModel.onActive()
        }

        override fun onInactive() {
            super.onInactive()
            this@ActionViewModel.onInactive()
        }
    }

    private var debounceJob: Job? = null
    private var previousAction: A? = null

    val uiState: LiveData<I> = _uiState.startWith(state)

    override val coroutineContext: CoroutineContext = baseContext + superJob
    protected abstract val processor: ActionProcessor<A>

    protected val logger by inject<ILogger>()

    init {
        initInternal()
    }

    private fun initInternal() {
        superJob.cancelChildren()
        actions = Channel(capacity = Channel.BUFFERED)
        launch {
            actions.consumeEach { action ->

                if (debounceActions <= 0L || previousAction != action) {
                    previousAction = action
                    processor.invoke(action)
                } else {
                    debounceJob?.cancel()
                    debounceJob = launch {
                        delay(debounceActions)
                        previousAction = action
                        processor.invoke(action)
                    }
                }
            }
        }
        changes = Channel(Channel.CONFLATED)
        launch {

            changes.consumeAsFlow()
                .distinctUntilChanged()
                .debounceIfPositive(debounceChanges)
                .collectLatest {
                    if (DBG) logger.d(TAG, "$state")
                    _uiState.postValue(state)
                }
        }

    }

    @CallSuper
    protected open fun onInit() {
        initInternal()
    }

    @HunterDebug
    fun dispatchAction(action: A) {
        actions.offer(action)
    }

    @JvmName("dispatchActionEx")
    protected fun A.dispatch() = dispatchAction(this)

    @HunterDebug
    @Synchronized
    protected fun emit(vararg change: Change<S>) {
        change.forEach { state = it.invoke(state) }
        state.emit()
    }

    protected fun Change<S>.emit() {
        emit(this)
    }

    protected fun S.emit() {
        state = this
        changes.offer(this)
    }

    override fun onCleared() {
        super.onCleared()
        superJob.cancel()
    }

    protected open fun onActive() {
        if (DBG) logger.d(TAG, "onActive")
    }

    protected open fun onInactive() {
        if (DBG) logger.d(TAG, "onInactive")
    }

    fun reInit() {
        onInit()
        if (uiState.hasActiveObservers()) onActive()
    }

    private fun handleFailure(failure: Flow<IFailure>, retryAction: RetryAction?) {
        launch {
            failure.collect {
                handleFailure(it, retryAction)
            }
        }
    }

    @CallSuper
    protected open fun handleFailure(failure: IFailure, retryAction: RetryAction?) {
        launch {
            failureDelegate.failureChannel.send(
                FailureObject(
                    failure,
                    { reInit() },
                    retryAction,
                    superJob
                )
            )
        }
    }

    protected fun <Params, Type> UseCase<Params, Type>.invoke(
        params: Params,
        onResult: (Type) -> Unit = {},
        onFailure: (IFailure, RetryAction?) -> Unit = ::handleFailure,
        retryAction: RetryAction? = null,
        scope: CoroutineScope = this@ActionViewModel

    ) where Type : Any =
        this.invoke(
            scope,
            params,
            onResult,
            { onFailure(it, retryAction) })


    protected fun <Params, Type> FlowUseCase<Params, Type>.invoke(
        params: Params,
        onResult: (Flow<Type>) -> Unit = {},
        onFailure: (Flow<IFailure>, RetryAction?) -> Unit = ::handleFailure,
        retryAction: RetryAction? = null,
        scope: CoroutineScope = this@ActionViewModel
    ) where Type : Any =
        this.invoke(scope, params, onResult, { onFailure(it, retryAction) })
}

fun <T> MutableLiveData<T>.startWith(state: T): LiveData<T> {
    this.postValue(state)
    return this
}

abstract class Change<S> {
    abstract operator fun invoke(s: S): S
}

fun <T> Flow<T>.debounceIfPositive(timeoutMillis: Long): Flow<T> {
    return if (timeoutMillis > 0) {
        this.debounce(timeoutMillis)
    } else {
        this
    }
}


