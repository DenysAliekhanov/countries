package com.example.countries.presentation_base

import com.example.countries.domain_base.logger.ILogger
import timber.log.Timber

class Logger(trees: List<Timber.Tree>): ILogger {
    init {
        trees.forEach {
            Timber.plant(it)
        }
    }

    override fun d(tag: String, message: String, vararg args: Any?) {
        Timber.tag(tag).d(message, args)
    }

    override fun v(tag: String, message: String, vararg args: Any?) {
        Timber.tag(tag).i(message, args)
    }

    override fun w(tag: String, message: String, vararg args: Any?) {
        Timber.tag(tag).w(message, args)
    }

    override fun i(tag: String, message: String, vararg args: Any?) {
        Timber.tag(tag).i(message, args)
    }

    override fun e(tag: String, t: Throwable?, message: String, vararg args: Any?) {
        Timber.tag(tag).e(t, message, args)
    }
}