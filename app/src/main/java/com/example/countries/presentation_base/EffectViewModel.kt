package com.example.countries.presentation_base

import androidx.lifecycle.LiveData
import kotlin.coroutines.CoroutineContext

abstract class EffectViewModel<I, S : I, A, E>(
    failureDelegate: IFailureDelegate,
    baseContext: CoroutineContext,
    initialState: S,
    debounceActions: Long = 0,
    debounceChanges: Long = 0
) : ActionViewModel<I, S, A>(failureDelegate, baseContext, initialState, debounceActions, debounceChanges) {

    private val _effect: SingleLiveEvent<E> =
        SingleLiveEvent()
    val effect: LiveData<E>
        get() = _effect

    @JvmName("dispatchEffectEx")
    protected fun E.effect() = _effect.postValue(this)
}