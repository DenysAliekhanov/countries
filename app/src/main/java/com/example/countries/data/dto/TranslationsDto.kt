package com.example.countries.data.dto

import com.example.countries.domain.entities.ITranslations
import com.google.gson.annotations.SerializedName

class TranslationsDto(
    @SerializedName("de") override val de: String?,
    @SerializedName("en") override val en: String?,
    @SerializedName("es") override val es: String?,
    @SerializedName("fr") override val fr: String?
): ITranslations