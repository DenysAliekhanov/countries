package com.example.countries.data.api

import com.example.countries.data.dto.CountryDto
import retrofit2.Call
import retrofit2.http.GET

private const val ALL = "/all"

interface Api {

  @GET(ALL)
  fun getAllCountries(): Call<List<CountryDto>>
}
