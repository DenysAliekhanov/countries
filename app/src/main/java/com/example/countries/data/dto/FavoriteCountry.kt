package com.example.countries.data.dto

import com.example.countries.domain.entities.IFavoriteCountry
import java.util.*

data class FavoriteCountry (
    override val id: String = UUID.randomUUID().toString(),
    override val name: String
): IFavoriteCountry