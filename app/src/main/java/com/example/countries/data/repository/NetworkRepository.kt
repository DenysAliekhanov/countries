package com.example.countries.data.repository

import com.example.countries.data.api.Api
import com.example.countries.data.api.Networking
import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.repositories.INetworkRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure

class NetworkRepository(private val api: Api, private val networking: Networking) :
    INetworkRepository {

    override suspend fun retrieveCountries(): Either<IFailure, List<ICountry>> {
        return networking.call(api.getAllCountries())
    }
}
