package com.example.countries.data.source

interface INetworkChecker {
    fun isConnected(): Boolean
}