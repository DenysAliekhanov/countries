package com.example.countries.data.dto

import com.example.countries.domain.entities.ICountry
import com.google.gson.annotations.SerializedName

data class CountryDto(
    @SerializedName("name") override val name: String?,
    @SerializedName("capital") override val capital: String?,
    @SerializedName("altSpellings") override val altSpellings: List<String>?,
    @SerializedName("relevance") override val relevance: String?,
    @SerializedName("region") override val region: String?,
    @SerializedName("subregion") override val subregion: String?,
    @SerializedName("translations") override val translations: TranslationsDto?,
    @SerializedName("population") override val population: Int?,
    @SerializedName("latlng") override val latlng: List<Double>?,
    @SerializedName("demonym") override val demonym: String?,
    @SerializedName("area") override val area: Float?,
    @SerializedName("gini") override val gini: Double?,
    @SerializedName("timezones") override val timezones: List<String>?,
    @SerializedName("callingCodes") override val callingCodes: List<String>?,
    @SerializedName("topLevelDomain") override val topLevelDomain: List<String>?,
    @SerializedName("alpha2Code") override val alpha2Code: String?,
    @SerializedName("alpha3Code") override val alpha3Code: String?,
    @SerializedName("currencies") override val currencies: List<String>?,
    @SerializedName("languages") override val languages: List<String>?,
    override var isFavorite: Boolean?
) : ICountry {
    override fun copyCountry(isFavorite: Boolean): ICountry {
        return this.copy(isFavorite = isFavorite)
    }
}