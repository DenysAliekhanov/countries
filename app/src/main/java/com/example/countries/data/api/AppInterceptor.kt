package com.example.countries.data.api

import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response

const val API_KEY = "856ddbd814msh15472a200bb0082p104be2jsn805c6c008529"

class AppInterceptor : Interceptor {

  override fun intercept(chain: Chain): Response {
    val request = chain.request()
    val builder = request.newBuilder()
    builder
        .header("x-rapidapi-host", "restcountries-v1.p.rapidapi.com")
        .header("x-rapidapi-key", API_KEY)
    return chain.proceed(builder.build())
  }
}
