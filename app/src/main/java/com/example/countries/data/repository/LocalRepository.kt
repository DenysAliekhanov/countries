package com.example.countries.data.repository

import com.example.countries.android_platform.data_base.CountriesDatabase
import com.example.countries.android_platform.data_base.countries.CountryEntity
import com.example.countries.data.dto.FavoriteCountry
import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.error.Failure
import com.example.countries.domain.error.StorageFailure
import com.example.countries.domain.error.StorageFailure.*
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.common.Either.*
import com.example.countries.domain_base.error.IFailure
import kotlin.Error

class LocalRepository(private val database: CountriesDatabase?) : ILocalRepository {

  private var sharedCountry: ICountry? = null

  override suspend fun getFavorites(): Either<Failure, List<IFavoriteCountry>> {
    val favorites = database?.countriesDao()?.selectAll()
    return when (favorites.isNullOrEmpty()) {
      true -> Error(DataNotFoundError("Data Not Found Error"))
      else -> Success(favorites.map { it.toLocalCountryEntity() })
    }
  }

  override suspend fun getFavoriteByName(name: String): Either<Failure, IFavoriteCountry> {
    val country = database?.countriesDao()?.getByName(name)
    return when (country == null) {
      true -> Error(DataNotFoundError())
      else -> Success(country.toLocalCountryEntity())
    }
  }

  override suspend fun insertFavorite(name: String): Success<Unit> {
    database?.countriesDao()?.insert(CountryEntity.fromLocalCountryEntity(FavoriteCountry(name = name)))
    return Success(Unit)
  }

  override suspend fun deleteFavorite(country: IFavoriteCountry): Success<Unit> {
    database?.countriesDao()?.delete(CountryEntity.fromLocalCountryEntity(country))
    return Success(Unit)
  }

  override suspend fun saveClickedCountry(country: ICountry): Success<Unit> {
    sharedCountry = country
    return Success(Unit)
  }

  override suspend fun getClickedCountry(): Either<IFailure, ICountry> {
    return sharedCountry?.let { Success(it) } ?: Error(StorageError("Shared Country is null"))
  }
}
