package com.example.countries.data.api

import com.example.countries.data.source.INetworkChecker
import com.example.countries.domain.error.Failure
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.logger.ILogger
import com.google.gson.Gson
import retrofit2.Call

class Networking(private val logger: ILogger, val networkChecker: INetworkChecker) {

    suspend inline fun <reified R> call(call: Call<R>): Either<IFailure, R> {
        if (!networkChecker.isConnected()) return Either.Error(Failure.NetworkConnection("NetworkConnection"))

        try {
            val response = call.execute()
            if(response.isSuccessful){
                return response.body()?.let {  Either.Success(it) } ?: Either.Error(Failure.HttpError("ResponseException"))
            }

            val gson = Gson()
            val errorResponse = gson.fromJson(response.errorBody()?.charStream(), ErrorMessage::class.java)

            return when (response.code()) {
                in 300..399 -> {
                    getErrorResponseResult(response.code(), "RedirectResponseException: ${errorResponse.message}")
                }
                in 400..499 -> {
                    getErrorResponseResult(response.code(), "ClientRequestException: ${errorResponse.message}")
                }
                in 500..599 -> {
                    getErrorResponseResult(response.code(), "ServerResponseException: ${errorResponse.message}")
                }
                else -> {
                    getErrorResponseResult(response.code(), "ResponseException: ${errorResponse.message}")
                }
            }
        } catch (e: Exception) {
            return Either.Error(Failure.HttpError(e.message ?: e.toString()))
        }
    }

    fun <R> getErrorResponseResult(status: Int, message: String): Either<IFailure, R> {
        println("call -> $status $message")
        return Either.Error(Failure.HttpError(message))
    }
}

data class ErrorMessage(val code: Int, val message: String)