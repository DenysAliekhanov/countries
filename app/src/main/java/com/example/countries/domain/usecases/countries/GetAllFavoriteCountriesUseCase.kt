package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.UseCase

class GetAllFavoriteCountriesUseCase(
    private val localRepository: ILocalRepository
) : UseCase<Unit, List<IFavoriteCountry>>() {

    override suspend fun run(params: Unit): Either<IFailure, List<IFavoriteCountry>> {
      return localRepository.getFavorites()
    }
}
