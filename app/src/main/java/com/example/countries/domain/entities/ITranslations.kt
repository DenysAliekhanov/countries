package com.example.countries.domain.entities

interface ITranslations {
    val de: String?
    val en: String?
    val es: String?
    val fr: String?
}