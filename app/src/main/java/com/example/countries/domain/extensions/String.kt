package com.example.countries.domain.extensions

fun String.Companion.empty() = ""
