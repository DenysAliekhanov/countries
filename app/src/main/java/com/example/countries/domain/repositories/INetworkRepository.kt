package com.example.countries.domain.repositories

import com.example.countries.domain.entities.ICountry
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure

interface INetworkRepository {
  suspend fun retrieveCountries(): Either<IFailure, List<ICountry>>
}
