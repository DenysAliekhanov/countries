package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.common.Either.*
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.UseCase

class ChangeFavoriteCountryUseCase(
    private val getFavoriteCountryByNameUseCase: GetFavoriteCountryByNameUseCase,
    private val createFavoriteCountryUseCase: CreateFavoriteCountryUseCase,
    private val deleteFavoriteCountryUseCase: DeleteFavoriteCountryUseCase
) : UseCase<String, IFavoriteCountry>() {

    override suspend fun run(params: String): Either<IFailure, IFavoriteCountry> {
      return when(val favoriteCountry = getFavoriteCountryByNameUseCase.run(params)) {
        is Error -> createFavoriteCountryUseCase.run(params)
        is Success -> deleteFavoriteCountryUseCase.run(favoriteCountry.successVal)
      }
    }
}
