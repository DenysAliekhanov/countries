package com.example.countries.domain.repositories

import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.common.Either.*
import com.example.countries.domain_base.error.IFailure

interface ILocalRepository {
  suspend fun getFavorites(): Either<IFailure, List<IFavoriteCountry>>
  suspend fun getFavoriteByName(name: String): Either<IFailure, IFavoriteCountry>
  suspend fun insertFavorite(name: String): Success<Unit>
  suspend fun deleteFavorite(country: IFavoriteCountry): Success<Unit>
  suspend fun saveClickedCountry(country: ICountry): Success<Unit>
  suspend fun getClickedCountry(): Either<IFailure, ICountry>
}
