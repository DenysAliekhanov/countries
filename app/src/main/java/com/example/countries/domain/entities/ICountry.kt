package com.example.countries.domain.entities

interface ICountry {
  val name: String?
  val capital: String?
  val altSpellings: List<String>?
  val relevance: String?
  val region: String?
  val subregion: String?
  val translations: ITranslations?
  val population: Int?
  val latlng: List<Double>?
  val demonym: String?
  val area: Float?
  val gini: Double?
  val timezones: List<String>?
  val callingCodes: List<String>?
  val topLevelDomain: List<String>?
  val alpha2Code: String?
  val alpha3Code: String?
  val currencies: List<String>?
  val languages: List<String>?
  var isFavorite: Boolean?

  fun copyCountry(isFavorite: Boolean): ICountry
}