package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.common.Either.*
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.UseCase

class SaveClickedCountryUseCase(
    private val localRepository: ILocalRepository
) : UseCase<ICountry, Unit>() {

    override suspend fun run(params: ICountry): Success<Unit> {
        return localRepository.saveClickedCountry(params)
    }
}
