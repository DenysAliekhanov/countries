package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.common.Either.*
import com.example.countries.domain_base.interactor.UseCase

class DeleteFavoriteCountryUseCase(
    private val localRepository: ILocalRepository
) : UseCase<IFavoriteCountry, IFavoriteCountry>() {

    override suspend fun run(params: IFavoriteCountry): Success<IFavoriteCountry> {
        localRepository.deleteFavorite(params)
        return Success(params)
    }
}
