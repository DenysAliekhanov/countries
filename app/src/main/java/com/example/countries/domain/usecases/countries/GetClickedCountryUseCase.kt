package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.UseCase

class GetClickedCountryUseCase(
    private val localRepository: ILocalRepository
) : UseCase<Unit, ICountry>() {

    override suspend fun run(params: Unit): Either<IFailure, ICountry> {
        return localRepository.getClickedCountry()
    }
}
