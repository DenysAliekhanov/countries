package com.example.countries.domain.entities

interface IFavoriteCountry{
  val id: String
  val name: String
}
