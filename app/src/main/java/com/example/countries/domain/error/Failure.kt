package com.example.countries.domain.error

import com.example.countries.domain_base.error.IFailure

sealed class Failure(override val errorMessage: String, val throwable: Throwable? = null) :
    IFailure {
    class IllegalArgument(msg: String = "") : Failure(msg)
    class NetworkConnection(msg: String = "") : Failure(msg)
    class HttpError(msg: String = "") : Failure(msg)
    class NotFoundError(msg: String = "") : Failure(msg)
}

sealed class StorageFailure(msg: String = "") : Failure(msg) {
    class DataNotFoundError(msg: String = "") : StorageFailure(msg)
    class StorageError(msg: String = "") : StorageFailure(msg)
}