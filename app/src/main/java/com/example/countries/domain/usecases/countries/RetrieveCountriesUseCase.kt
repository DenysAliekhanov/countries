package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.repositories.INetworkRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.common.Either.*
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.UseCase

private const val REQUEST_DELAY = 400L

class RetrieveCountriesUseCase(
  private val networkRepository: INetworkRepository,
  private val getAllFavoriteCountriesUseCase: GetAllFavoriteCountriesUseCase
) : UseCase<Unit, List<ICountry>>() {

  override val requestDelay = REQUEST_DELAY

  override suspend fun run(params: Unit): Either<IFailure, List<ICountry>> {
    return when(val countries = networkRepository.retrieveCountries()) {
      is Error -> countries
      is Success -> getAllFavoriteCountries(countries.successVal)
    }
  }

  private suspend fun getAllFavoriteCountries(countries: List<ICountry>): Either<IFailure, List<ICountry>> {
    return when (val allFavoriteCountries = getAllFavoriteCountriesUseCase.run(Unit)) {
      is Error -> Success(countries)
      is Success -> mergeCountries(countries, allFavoriteCountries.successVal)
    }
  }

  private fun mergeCountries(countries: List<ICountry>, favoritesCountries: List<IFavoriteCountry>): Success<List<ICountry>> {
    val mergedCountries = countries.map {
      it.copyCountry(favoritesCountries.map { fav -> fav.name }.contains(it.name))
    }
    return Success(mergedCountries)
  }
}
