package com.example.countries.domain.usecases.countries

import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.repositories.ILocalRepository
import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure
import com.example.countries.domain_base.interactor.UseCase

class GetFavoriteCountryByNameUseCase(private val localRepository: ILocalRepository) :
    UseCase<String, IFavoriteCountry>() {

    override suspend fun run(params: String): Either<IFailure, IFavoriteCountry> {
        return localRepository.getFavoriteByName(params)
    }
}
