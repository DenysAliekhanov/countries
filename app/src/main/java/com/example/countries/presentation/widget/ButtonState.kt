package com.example.countries.presentation.widget

enum class ButtonState { ACTIVE, INACTIVE, IN_PROGRESS }