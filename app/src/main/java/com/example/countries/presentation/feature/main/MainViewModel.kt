package com.example.countries.presentation.feature.main

import android.content.Context
import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.entities.IFavoriteCountry
import com.example.countries.domain.usecases.countries.ChangeFavoriteCountryUseCase
import com.example.countries.domain.usecases.countries.RetrieveCountriesUseCase
import com.example.countries.domain.usecases.countries.SaveClickedCountryUseCase
import com.example.countries.presentation.feature.main.MainViewModel.MainAction
import com.example.countries.presentation.feature.main.MainViewModel.MainAction.*
import com.example.countries.presentation.feature.main.MainViewModel.MainEffect
import com.example.countries.presentation.feature.main.MainViewModel.MainEffect.OpenDetailsScreen
import com.example.countries.presentation_base.ActionProcessor
import com.example.countries.presentation_base.EffectViewModel
import com.example.countries.presentation_base.IFailureDelegate
import kotlin.coroutines.CoroutineContext

class MainViewModel(
    private val appContext: Context,
    baseContext: CoroutineContext,
    failureDelegate: IFailureDelegate,
    initialState: MainState = MainState(),
    private val retrieveCountriesUseCase: RetrieveCountriesUseCase,
    private val changeFavoriteCountryUseCase: ChangeFavoriteCountryUseCase,
    private val saveClickedCountryUseCase: SaveClickedCountryUseCase
) :
    EffectViewModel<IMainView, MainState, MainAction, MainEffect>(
        failureDelegate,
        baseContext,
        initialState
    ) {

    override val processor: ActionProcessor<MainAction> = { action ->
        when (action) {
            is RetrieveCountries -> retrieveCountries()
            is CountryClick -> handleCountryClick(action.country)
            is FavoriteClick -> changeFavorite(action.country.name)
            is FilterTextChanged -> sortCountriesList(action.text)
        }
    }

    private fun handleCountryClick(country: ICountry) {
        saveClickedCountryUseCase.invoke(country, { OpenDetailsScreen.effect() })
    }

    private fun handleCountriesResponse(countriesList: List<ICountry>) {
        state.copy(isProgress = false, countryList = countriesList, sortedList = countriesList)
            .emit()
    }

    private fun retrieveCountries() {
        state.copy(isProgress = true).emit()
        retrieveCountriesUseCase.invoke(
            Unit,
            onResult = { handleCountriesResponse(it) },
            retryAction = { retrieveCountries() })
    }

    private fun changeFavorite(name: String?) {
        name?.let { changeFavoriteCountryUseCase.invoke(it, ::changeFavoriteSucceed) }
    }

    private fun changeFavoriteSucceed(localCountry: IFavoriteCountry) {
        val sortedList = state.sortedList.map { setFavoriteCountry(it, localCountry) }
        val country = state.countryList.map { setFavoriteCountry(it, localCountry) }
        state.copy(countryList = country, sortedList = sortedList).emit()
    }

    private fun setFavoriteCountry(country: ICountry, localCountry: IFavoriteCountry): ICountry {
        return if (country.name == localCountry.name) {
            country.copyCountry(!(country.isFavorite ?: false))
        } else {
            country
        }
    }

    private fun sortCountriesList(text: String) {
        val counties = state.countryList
        val sortedList = if (counties.isNullOrEmpty()) {
            emptyList()
        } else {
            counties.filter { it.name?.contains(text, ignoreCase = true) == true }
        }
        state.copy(sortedList = sortedList).emit()
    }

    sealed class MainAction {
        object RetrieveCountries : MainAction()
        data class CountryClick(val country: ICountry) : MainAction()
        data class FavoriteClick(val country: ICountry) : MainAction()
        data class FilterTextChanged(val text: String) : MainAction()
    }

    sealed class MainEffect {
        object OpenDetailsScreen : MainEffect()
    }
}
