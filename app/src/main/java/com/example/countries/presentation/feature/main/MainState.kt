package com.example.countries.presentation.feature.main

import com.example.countries.domain.entities.ICountry

interface IMainView {
    val isProgress: Boolean
    val countryList: List<ICountry>
    val sortedList: List<ICountry>
}

data class MainState(
    override val isProgress: Boolean = false,
    override val countryList: List<ICountry> = emptyList(),
    override val sortedList: List<ICountry> = emptyList()
) : IMainView
