package com.example.countries.presentation.feature.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.countries.R
import com.example.countries.presentation.core.BaseFragment
import com.example.countries.presentation.feature.details.DetailsViewModel.DetailsAction.*
import com.example.countries.presentation.feature.details.DetailsViewModel.DetailsEffect.OpenPreviousScreen
import com.example.countries.presentation_base.observe
import kotlinx.android.synthetic.main.fragment_details.*
import org.koin.android.viewmodel.ext.android.viewModel


class DetailsFragment : BaseFragment(), DetailsView.CallBack {

    private val viewModel: DetailsViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
      detailsView.callback = this

        observe(viewModel.uiState) {
            detailsView.updateUi(it)
        }

        observe(viewModel.effect) {
            when (it) {
                is OpenPreviousScreen -> activity?.onBackPressed()
            }
        }

        viewModel.dispatchAction(RetrieveCountry)
    }

  override fun backButtonClicked() = viewModel.dispatchAction(BackClicked)

}
