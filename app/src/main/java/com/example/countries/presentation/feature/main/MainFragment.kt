package com.example.countries.presentation.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.countries.R
import com.example.countries.domain.entities.ICountry
import com.example.countries.presentation.core.BaseFragment
import com.example.countries.presentation.feature.main.MainView.CallBack
import com.example.countries.presentation.feature.main.MainViewModel.MainAction.*
import com.example.countries.presentation.feature.main.MainViewModel.MainEffect.OpenDetailsScreen
import com.example.countries.presentation_base.observe
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment(), CallBack {

  private val viewModel: MainViewModel by viewModel()

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_main, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    mainView.callback = this

    observe(viewModel.uiState) {
      mainView.updateUi(it)
    }

    observe(viewModel.effect) {
      when (it) {
        is OpenDetailsScreen -> { navigateTo(R.id.action_mainFragment_to_detailsFragment, R.id.mainFragment) }
      }
    }

    viewModel.dispatchAction(RetrieveCountries)
  }

  override fun filterTextChanged(text: String) = viewModel.dispatchAction(FilterTextChanged(text))

  override fun countryClicked(country: ICountry) = viewModel.dispatchAction(CountryClick(country))

  override fun favoriteClicked(country: ICountry) = viewModel.dispatchAction(FavoriteClick(country))
}
