package com.example.countries.presentation.feature.main

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.countries.R
import com.example.countries.domain.entities.ICountry
import com.example.countries.presentation.extensions.invisible
import com.example.countries.presentation.extensions.visible
import com.example.countries.presentation.feature.main.MainAdapter.*
import kotlinx.android.synthetic.main.view_main.view.*


class MainView @JvmOverloads constructor(context: Context,
                                                  attrs: AttributeSet? = null,
                                                  defStyleAttr: Int = 0) :
    ConstraintLayout(context, attrs, defStyleAttr), CountriesAdapterListener{

    var callback: CallBack? = null

    private var adapter: MainAdapter? = null

    init {
        View.inflate(context, R.layout.view_main, this)
        initCountriesList()
        initSearch()
    }

    fun updateUi(model: IMainView) {
        if (model.isProgress) progress.visible() else progress.invisible()
        adapter?.setCountries(model.sortedList.toMutableList())
    }

    private fun initCountriesList() {
        adapter = MainAdapter(this)
        countries.adapter = adapter
    }

    private fun initSearch() {
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                callback?.filterTextChanged(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })
    }

    override fun countryClicked(country: ICountry) {
        callback?.countryClicked(country)
    }

    override fun favoriteClicked(country: ICountry) {
        callback?.favoriteClicked(country)
    }

    interface CallBack {
        fun filterTextChanged(text: String)
        fun countryClicked(country: ICountry)
        fun favoriteClicked(country: ICountry)
    }
}
