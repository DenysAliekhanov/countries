package com.example.countries.presentation.feature.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.countries.R
import com.example.countries.domain.entities.ICountry
import com.example.countries.presentation.core.BaseDiff
import kotlinx.android.synthetic.main.item_country.view.*

class MainAdapter(private val listener: CountriesAdapterListener?) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val countries: MutableList<ICountry> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false)
        )
    }

    override fun getItemCount(): Int = countries.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(countries[position])
    }

    fun setCountries(countries: MutableList<ICountry>) {
        val countriesDiff = CountriesDiff(this.countries, countries)
        val diffResults = DiffUtil.calculateDiff(countriesDiff)
        this.countries.clear()
        this.countries.addAll(countries)
        diffResults.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(country: ICountry) = with(itemView) {
            name.text = if (country.name?.isNotEmpty() == true) country.name else name.text.toString()
            capital.text = if (country.capital?.isNotEmpty() == true) country.capital else capital.text.toString()
            region.text = if (country.region?.isNotEmpty()  == true) country.region else region.text.toString()
            val favoriteIndicator = if (country.isFavorite == true) R.drawable.ic_favorite_heart_full else R.drawable.ic_favorite_heart
            favorite.setImageResource(favoriteIndicator)
            itemView.setOnClickListener { listener?.countryClicked(country) }
          country.name?.let { name ->  favorite.setOnClickListener { listener?.favoriteClicked(country) } }
        }
    }

    class CountriesDiff(
        private val oldList: List<ICountry>,
        private val newList: List<ICountry>
    ) : BaseDiff<ICountry>(oldList, newList) {

        override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            return oldList[oldPosition].name == newList[newPosition].name
        }
    }

    interface CountriesAdapterListener {
        fun countryClicked(country: ICountry)
        fun favoriteClicked(country: ICountry)
    }
}
