package com.example.countries.presentation.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.DisplayMetrics
import android.widget.Toast

val Context.networkInfo: NetworkInfo?
  get() = (this.getSystemService(Context.CONNECTIVITY_SERVICE)
      as ConnectivityManager).activeNetworkInfo

fun Context.toast(text: String?, duration: Int = Toast.LENGTH_SHORT) =
  Toast.makeText(this, text.orEmpty(), duration).show()

fun Context.dpToPx(dp: Int): Int = (dp * this.displayMetrics().density + 0.5f).toInt()

fun Context.displayMetrics(): DisplayMetrics = this.resources.displayMetrics
