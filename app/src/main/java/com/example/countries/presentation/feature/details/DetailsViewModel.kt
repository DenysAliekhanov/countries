package com.example.countries.presentation.feature.details

import android.content.Context
import com.example.countries.R
import com.example.countries.domain.entities.ICountry
import com.example.countries.domain.extensions.empty
import com.example.countries.domain.usecases.countries.GetClickedCountryUseCase
import com.example.countries.presentation.feature.details.DetailsViewModel.DetailsAction
import com.example.countries.presentation.feature.details.DetailsViewModel.DetailsAction.*
import com.example.countries.presentation.feature.details.DetailsViewModel.DetailsEffect
import com.example.countries.presentation.feature.details.DetailsViewModel.DetailsEffect.*
import com.example.countries.presentation_base.ActionProcessor
import com.example.countries.presentation_base.EffectViewModel
import com.example.countries.presentation_base.IFailureDelegate
import kotlin.coroutines.CoroutineContext

class DetailsViewModel(
    private val appContext: Context,
    baseContext: CoroutineContext,
    failureDelegate: IFailureDelegate,
    initialState: DetailsState = DetailsState(),
    private val getClickedCountryUseCase: GetClickedCountryUseCase
) :
    EffectViewModel<IDetailsView, DetailsState, DetailsAction, DetailsEffect>(
        failureDelegate,
        baseContext,
        initialState
    ) {

    override val processor: ActionProcessor<DetailsAction> = { action ->
        when (action) {
            is RetrieveCountry -> getClickedCountryUseCase.invoke(Unit, ::setCountry)
            is BackClicked -> OpenPreviousScreen.effect()
        }
    }

    private fun setCountry(country: ICountry) {
        state.copy(
            name = country.name ?: appContext.getString(R.string.no_information),
            capital = country.capital ?: appContext.getString(R.string.no_information),
            region = country.region ?: appContext.getString(R.string.no_information),
            subRegion = country.subregion ?: appContext.getString(R.string.no_information),
            population = if (country.population == null || country.population == DEFAULT_VALUE)
                appContext.getString(R.string.no_information) else country.population.toString(),
            languages = getLanguages(country),
            relevance = country.relevance ?: appContext.getString(R.string.no_information),
            isFavorite = country.isFavorite ?: false
        ).emit()
    }

    private fun getLanguages(country: ICountry?): String {
        return country?.languages?.let { list ->
            var text = list.firstOrNull() ?: String.empty()
            list.forEachIndexed { index, it ->
                text = if (index == 0) {
                    text
                } else {
                    "$text\n$it"
                }
            }
            text
        } ?: appContext.getString(R.string.no_information)
    }

    sealed class DetailsAction {
        object RetrieveCountry : DetailsAction()
        object BackClicked : DetailsAction()
    }

    sealed class DetailsEffect {
        object OpenPreviousScreen : DetailsEffect()
    }
}
