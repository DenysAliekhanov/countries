package com.example.countries.presentation.feature.details

interface IDetailsView {
    val name: String
    val capital: String
    val region: String
    val subRegion: String
    val population: String
    val relevance: String
    val languages: String
    val isFavorite: Boolean
}

data class DetailsState(
    override val name: String = "",
    override val capital: String = "",
    override val region: String = "",
    override val subRegion: String = "",
    override val population: String = "",
    override val relevance: String = "",
    override val languages: String = "",
    override val isFavorite: Boolean = false
) : IDetailsView
