package com.example.countries.presentation.core

import android.content.Context
import com.example.countries.presentation.dialogs.FailureAlertDialog
import com.example.countries.presentation.extensions.toast
import com.example.countries.presentation_base.FailureObject
import com.example.countries.presentation_base.IFailureDelegate
import com.example.countries.presentation_base.RetryAction
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


class MainFailureHandler(
    private val appLifecycle: IAppLifecycle,
    private val appContext: Context
) : IFailureDelegate, CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main + Job()
    override val failureChannel: Channel<FailureObject> = Channel()


    companion object {
        private val TAG = "MainFailureHandler"
    }

    init {
        launch {
            failureChannel.consumeAsFlow().debounce(20).collectLatest(::processFailure)
        }
    }

    private suspend fun processFailure(failureObject: FailureObject) {
                val retryAction = failureObject.retryAction
                if (retryAction != null) {
                    showFailureRetryAlert(failureObject.failure.errorMessage, retryAction)
                } else {
                    showFailureToast(failureObject.failure.errorMessage)
                }
        }

    private suspend fun showFailureToast(message: String) {
        appContext.toast(message)
    }

    private suspend fun showFailureRetryAlert(message: String, retryAction: RetryAction) {

        appLifecycle.foregroundActivity?.let {
            FailureAlertDialog(it, message, retryAction).show()
        }
    }
}