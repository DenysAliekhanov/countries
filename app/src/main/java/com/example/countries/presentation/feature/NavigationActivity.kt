package com.example.countries.presentation.feature

import android.os.Bundle
import com.example.countries.R
import com.example.countries.presentation.core.BaseActivity

class NavigationActivity : BaseActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_navigation)
  }
}
