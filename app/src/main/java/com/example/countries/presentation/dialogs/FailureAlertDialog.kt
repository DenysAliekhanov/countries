package com.example.countries.presentation.dialogs

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import com.example.countries.R
import com.example.countries.presentation_base.RetryAction
import kotlinx.android.synthetic.main.retry_dialog_error.view.*

class FailureAlertDialog(
    context: Context,
    errorMessage: String,
    retryAction: RetryAction,
    dismissListener: (() -> Unit)? = null
) {
    companion object {
        private const val TAG = "FailureAlertDialog"
    }

    private val dialog: AlertDialog

    init {
        val alertView =
            LayoutInflater.from(context).inflate(R.layout.retry_dialog_error, null, false)
        alertView.error_message.text = errorMessage
        dialog = AlertDialog.Builder(context)
            .setView(alertView)
            .setOnDismissListener {
                dismissListener?.invoke()
            }
            .create()

        // do not cancel when click outside
        dialog.setCanceledOnTouchOutside(false)

        //move dialog to right top corner
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window?.let {
            it.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        alertView.try_again_btn.setOnClickListener {
            dialog.dismiss()
            retryAction.invoke()
        }
        alertView.ignore_btn.setOnClickListener { dialog.dismiss() }
    }

    fun show() {
        dialog.show()
    }
}