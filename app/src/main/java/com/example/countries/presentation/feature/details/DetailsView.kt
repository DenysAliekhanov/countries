package com.example.countries.presentation.feature.details

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.countries.R
import com.example.countries.presentation.extensions.gone
import com.example.countries.presentation.extensions.singleClickListener
import com.example.countries.presentation.extensions.visible
import kotlinx.android.synthetic.main.view_details.view.*

const val DEFAULT_VALUE = 0

class DetailsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    var callback: CallBack? = null

    init {
        View.inflate(context, R.layout.view_details, this)
        backBtn.singleClickListener { callback?.backButtonClicked() }
    }

    fun updateUi(model: IDetailsView) {
        name.text = model.name
        capital.text = model.capital
        region.text = model.region
        subregion.text = model.subRegion
        population.text = model.population
        languages.text = model.languages
        relevance.text = model.relevance
        if (model.isFavorite) {
            favorite.visible()
        } else {
            favorite.gone()
        }
    }

    interface CallBack {
        fun backButtonClicked()
    }
}
