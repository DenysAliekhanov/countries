package com.example.countries.presentation.core

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

abstract class BaseFragment: Fragment() {

    protected val TAG = this::class.java.simpleName

    protected fun navigateTo(id: Int, currentFragment: Int) {
        val navController = findNavController()
        if (navController.currentDestination?.id == currentFragment) {
            navController.navigate(id)
        }
    }

}