package com.example.countries.presentation.core

import androidx.appcompat.app.AppCompatActivity

interface IAppLifecycle {

    val foregroundActivity: AppCompatActivity?

}