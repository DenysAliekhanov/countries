package com.example.countries.presentation.widget

import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.countries.R
import com.example.countries.presentation.extensions.disable
import com.example.countries.presentation.extensions.dpToPx
import com.example.countries.presentation.extensions.enable

class TextButton @JvmOverloads constructor(context: Context,
                                           attrs: AttributeSet? = null,
                                           defStyleAttr: Int = 0) :
    TextView(context, attrs, defStyleAttr) {

    private var strokeWidth: Float = 10f
    private val outlinePaint = Paint()
    private val progressPaint = Paint()
    private var outlinePath = Path()
    private var progressPath = Path()
    private var pathPadding: Float = strokeWidth / 2
    private var pathMeasure = PathMeasure(outlinePath, false)
    private var progressStart: Float = 0f
    private var progressEnd: Float = 0f
    private var progressPathIncrease = true
    private var animator: ValueAnimator
    private var showOutline = false
    private var showProgress = false
    private var state = ButtonState.ACTIVE
    private var backgroundActive: Drawable? = null
    private var backgroundInactive: Drawable? = null

    init {
        animator = createAnimator()
        initDefaultConfiguration()
        initAttributes(attrs)
        applyButtonState()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        calculatePath(w, h)
        applyProgress()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (showOutline) canvas.drawPath(outlinePath, outlinePaint)
        if (showProgress) canvas.drawPath(progressPath, progressPaint)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        stopAnimation()
    }

    private fun initDefaultConfiguration() {
        strokeWidth = context.dpToPx(2).toFloat()
        pathPadding = strokeWidth / 2

        outlinePaint.color = ContextCompat.getColor(context, R.color.color_background_progress)
        outlinePaint.isAntiAlias = true
        outlinePaint.style = Paint.Style.STROKE
        outlinePaint.strokeWidth = strokeWidth
        outlinePaint.strokeJoin = Paint.Join.ROUND
        outlinePaint.strokeCap = Paint.Cap.ROUND

        progressPaint.color = ContextCompat.getColor(context, R.color.red_chalk)
        progressPaint.isAntiAlias = true
        progressPaint.style = Paint.Style.STROKE
        progressPaint.strokeWidth = strokeWidth
        progressPaint.strokeJoin = Paint.Join.ROUND
        progressPaint.strokeCap = Paint.Cap.ROUND
    }

    private fun initAttributes(attrs: AttributeSet?) {
        attrs?.let {
            val arr = context.obtainStyledAttributes(attrs, R.styleable.TextButton)
            try {
                if (arr.hasValue(R.styleable.TextButton_tb_outline_color)) {
                    outlinePaint.color = arr.getColor(R.styleable.TextButton_tb_outline_color, outlinePaint.color)
                }

                if (arr.hasValue(R.styleable.TextButton_tb_progress_color)) {
                    progressPaint.color = arr.getColor(R.styleable.TextButton_tb_progress_color, progressPaint.color)
                }

                if (arr.hasValue(R.styleable.TextButton_tb_progress_stroke_width)) {
                    strokeWidth = arr.getDimensionPixelSize(R.styleable.TextButton_tb_progress_stroke_width, strokeWidth.toInt()).toFloat()
                    pathPadding = strokeWidth / 2
                    outlinePaint.strokeWidth = strokeWidth
                    progressPaint.strokeWidth = strokeWidth
                }

                if (arr.hasValue(R.styleable.TextButton_tb_show_outline)) {
                    showOutline = arr.getBoolean(R.styleable.TextButton_tb_show_outline, showOutline)
                }


                if (arr.hasValue(R.styleable.TextButton_tb_background_active)) {
                    backgroundActive = arr.getDrawable(R.styleable.TextButton_tb_background_active)
                }

                if (arr.hasValue(R.styleable.TextButton_tb_background_inactive)) {
                    backgroundInactive = arr.getDrawable(R.styleable.TextButton_tb_background_inactive)
                }

            } finally {
                arr.recycle()
            }
        }
    }

    fun setButtonState(buttonState: ButtonState) {
        if (state != buttonState) {
            state = buttonState
            applyButtonState()
        }
    }

    private fun applyButtonState() {
        when(state) {
            ButtonState.ACTIVE -> {
                background = backgroundActive
                enable()
                hideProgress()
            }
            ButtonState.INACTIVE -> {
                background = backgroundInactive
                disable()
                hideProgress()
            }
            ButtonState.IN_PROGRESS -> {
                background = backgroundInactive
                disable()
                showProgress()
            }
        }
    }

    private fun showProgress() {
        showProgress = true
        startAnimation()
    }

    private fun hideProgress() {
        showProgress = false
        stopAnimation()
        invalidate()
    }

    private fun calculatePath(w: Int, h: Int) {
        val availableHeight = h - pathPadding * 2
        val availableWidth = w - pathPadding * 2
        val radius = availableHeight / 2

        outlinePath.reset()
        outlinePath.moveTo(w / 2f, pathPadding)
        outlinePath.lineTo(availableWidth + pathPadding - radius, pathPadding)
        outlinePath.arcTo(availableWidth + pathPadding - availableHeight, pathPadding, availableWidth + pathPadding, availableHeight + pathPadding, -90f,  180f, false)
        outlinePath.lineTo(radius + pathPadding, availableHeight + pathPadding)
        outlinePath.arcTo(pathPadding, pathPadding, availableHeight + pathPadding, availableHeight + pathPadding, 90f,  180f, false)
        outlinePath.lineTo(w / 2f, pathPadding)
        outlinePath.close()

        pathMeasure = PathMeasure(outlinePath, false)
    }

    private fun addProgress(incrementStart: Float, incrementEnd: Float) {
        progressStart += if (progressStart + incrementStart <= PROGRESS_MAX) incrementStart else incrementStart - PROGRESS_MAX
        progressEnd += if (progressEnd  + incrementEnd <= PROGRESS_MAX) incrementEnd else incrementEnd - PROGRESS_MAX
        applyProgress()
        invalidate()
    }

    private fun applyProgress() {
        progressPath.reset()
        val startD = pathMeasure.length * progressStart / PROGRESS_MAX
        val stopD = pathMeasure.length * progressEnd / PROGRESS_MAX
        if (startD > stopD) {
            pathMeasure.getSegment(startD, pathMeasure.length, progressPath, true)
            pathMeasure.getSegment(0f, stopD, progressPath, true)
        } else {
            pathMeasure.getSegment(startD, stopD, progressPath, true)
        }
    }

    private fun createAnimator(): ValueAnimator {
        val animator = ValueAnimator()
        animator.setValues(
            PropertyValuesHolder.ofInt("progress",
                PROGRESS_MIN,
                PROGRESS_MAX
            ))
        animator.duration = 2000
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.repeatCount = ValueAnimator.INFINITE
        animator.setTarget(this)

        animator.addUpdateListener {
            if (progressPathIncrease) {
                addProgress(
                    PROGRESS_INCREMENT_MIN,
                    PROGRESS_INCREMENT_MAX
                )
                if (getProgressLength() >= PROGRESS_LENGTH_MAX) progressPathIncrease = false

            } else {
                addProgress(
                    PROGRESS_INCREMENT_MAX,
                    PROGRESS_INCREMENT_MIN
                )
                if (getProgressLength() <= PROGRESS_LENGTH_MIN) progressPathIncrease = true
            }
        }

        return animator
    }

    private fun getProgressLength() : Float {
        return if (progressEnd > progressStart) {
            progressEnd - progressStart
        } else {
            progressEnd - progressStart + PROGRESS_MAX
        }
    }

    private fun startAnimation() {
        if(!animator.isRunning) animator.start()
    }

    private fun stopAnimation() {
        if(animator.isRunning) animator.cancel()
    }

    companion object {
        const val PROGRESS_MIN = 0
        const val PROGRESS_MAX = 100
        const val PROGRESS_INCREMENT_MIN = 1f
        const val PROGRESS_INCREMENT_MAX = 3f
        const val PROGRESS_LENGTH_MIN = 10f
        const val PROGRESS_LENGTH_MAX = 90f
    }

    enum class State { ACTIVE, INACTIVE, IN_PROGRESS }

}