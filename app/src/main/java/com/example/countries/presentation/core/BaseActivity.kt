package com.example.countries.presentation.core

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.countries.R
import com.example.countries.domain_base.logger.ILogger
import org.koin.android.ext.android.inject


abstract class BaseActivity : AppCompatActivity() {

    protected val TAG = this::class.java.simpleName

    protected val logger: ILogger by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var statusBarColor = ContextCompat.getColor(this, R.color.white_dirty_color)
        var navigationBarColor = ContextCompat.getColor(this, R.color.white_dirty_color)
        var isLightStatusBarColor = false
        var isLightNavigationBarColor = true

        val ta = obtainStyledAttributes(R.styleable.ActivityAttributes)
        try {
            if (ta.hasValue(R.styleable.ActivityAttributes_activity_status_bar_color)) {
                statusBarColor = ta.getColor(R.styleable.ActivityAttributes_activity_status_bar_color, statusBarColor)
            }
            if (ta.hasValue(R.styleable.ActivityAttributes_activity_is_light_status_bar_color)) {
                isLightStatusBarColor = ta.getBoolean(R.styleable.ActivityAttributes_activity_is_light_status_bar_color, isLightStatusBarColor)
            }
            if (ta.hasValue(R.styleable.ActivityAttributes_activity_navigation_bar_color)) {
                navigationBarColor = ta.getColor(R.styleable.ActivityAttributes_activity_navigation_bar_color, navigationBarColor)
            }
            if (ta.hasValue(R.styleable.ActivityAttributes_activity_is_light_navigation_bar_color)) {
                isLightNavigationBarColor = ta.getBoolean(R.styleable.ActivityAttributes_activity_is_light_navigation_bar_color, isLightNavigationBarColor)
            }
        } finally {
            ta.recycle()
        }

        setWindowDecorColors(statusBarColor, navigationBarColor, isLightStatusBarColor, isLightNavigationBarColor)

    }

    @IdRes
    protected open fun getContentLayoutId(): Int {
        return -1
    }

    protected open fun setWindowDecorColors(@ColorInt statusBarColor: Int?,
                                            @ColorInt navigationBarColor: Int?,
                                            isLightStatus: Boolean,
                                            isLightNavigation: Boolean) {

        if (navigationBarColor != null) {
            window.navigationBarColor = navigationBarColor
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val view = window.decorView
            var flagsNavigation = view.systemUiVisibility
            flagsNavigation = if (isLightNavigation && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                flagsNavigation or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                View.SYSTEM_UI_FLAG_VISIBLE
            }

            applyWindowDecorViewFlags(window.decorView, flagsNavigation, isLightStatus, isLightNavigation)
        }

        if (statusBarColor != null) {
            window.statusBarColor = statusBarColor
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var flagsStatus = window.decorView.systemUiVisibility
            flagsStatus = if (isLightStatus && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                flagsStatus or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                View.SYSTEM_UI_FLAG_VISIBLE
            }

            applyWindowDecorViewFlags(window.decorView, flagsStatus, isLightStatus, isLightNavigation)
        }

    }

    protected open fun applyWindowDecorViewFlags(windowDecorView: View,
                                                 flags: Int,
                                                 isLightStatus: Boolean,
                                                 isLightNavigation: Boolean) {
        windowDecorView.systemUiVisibility = flags
        if (!isLightStatus && isLightNavigation && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }

}