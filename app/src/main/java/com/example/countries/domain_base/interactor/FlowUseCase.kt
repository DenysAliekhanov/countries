package com.example.countries.domain_base.interactor

import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch

abstract class FlowUseCase<in Params, out Type> where Type : Any? {

    abstract suspend fun run(params: Params): Flow<Either<IFailure, Type>>

    open operator fun invoke(
        scope: CoroutineScope,
        params: Params,
        onResult: (Flow<Type>) -> Unit = {},
        onFailure: (Flow<IFailure>) -> Unit = {}
    ) {
        scope.launch {

            val successChannel = Channel<Type>()
            val errorChannel = Channel<IFailure>()

            onResult(successChannel.consumeAsFlow())
            onFailure(errorChannel.consumeAsFlow())

            run(params).collect {
                when (it) {
                    is Either.Error -> {
                        errorChannel.send(it.errorVal)
                    }
                    is Either.Success -> {
                        successChannel.send(it.successVal)
                    }
                }
            }
        }
    }
}