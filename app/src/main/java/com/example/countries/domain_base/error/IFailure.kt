package com.example.countries.domain_base.error

interface IFailure {
    val errorMessage: String
}