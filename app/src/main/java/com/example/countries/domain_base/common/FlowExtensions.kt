package com.example.countries.domain_base.common

import com.example.countries.domain_base.error.IFailure
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.combineTransform


/**
 * Extension to [combine] that handles Either.Error cases.
 */
fun <T1, T2, R> Flow<Either<IFailure, T1>>.combineLatestWithError(
    other: Flow<Either<IFailure, T2>>,
    transform: suspend (T1, T2) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> =
    this.combine(other) { t1: Either<IFailure, T1>, t2: Either<IFailure, T2> ->
        when (t1) {
            is Either.Error -> t1
            is Either.Success -> when (t2) {
                is Either.Error -> t2
                is Either.Success -> transform(t1.successVal, t2.successVal)
            }
        }
    }

/**
 * Extension to [combine] that handles Either.Error cases.
 */
fun <T1, T2, T3, R> combineWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    flow3: Flow<Either<IFailure, T3>>,
    transform: suspend (T1, T2, T3) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> = combine(flow1, flow2, flow3) { t1, t2, t3 ->
    when (t1) {
        is Either.Error -> t1
        is Either.Success -> when (t2) {
            is Either.Error -> t2
            is Either.Success -> when (t3) {
                is Either.Error -> t3
                is Either.Success ->
                    transform(
                        t1.successVal,
                        t2.successVal,
                        t3.successVal
                    )
            }
        }
    }
}

/**
 * Extension to [combine] that handles Either.Error cases.
 */
fun <T1, T2, T3, T4, R> combineWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    flow3: Flow<Either<IFailure, T3>>,
    flow4: Flow<Either<IFailure, T4>>,
    transform: suspend (T1, T2, T3, T4) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> = combine(flow1, flow2, flow3, flow4) { t1, t2, t3, t4->
    when (t1) {
        is Either.Error -> t1
        is Either.Success -> when (t2) {
            is Either.Error -> t2
            is Either.Success -> when (t3) {
                is Either.Error -> t3
                is Either.Success -> when (t4){
                    is Either.Error -> t4
                    is Either.Success -> {
                        transform(
                            t1.successVal,
                            t2.successVal,
                            t3.successVal,
                            t4.successVal
                        )
                    }
                }
            }
        }
    }
}

/**
 * Extension to [combine] that handles Either.Error cases.
 */
fun <T1, T2, T3, T4, T5, R> combineWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    flow3: Flow<Either<IFailure, T3>>,
    flow4: Flow<Either<IFailure, T4>>,
    flow5: Flow<Either<IFailure, T5>>,
    transform: suspend (T1, T2, T3, T4, T5) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> = combine(flow1, flow2, flow3, flow4, flow5) { t1, t2, t3, t4, t5 ->
    when (t1) {
        is Either.Error -> t1
        is Either.Success -> when (t2) {
            is Either.Error -> t2
            is Either.Success -> when (t3) {
                is Either.Error -> t3
                is Either.Success -> when (t4) {
                    is Either.Error -> t4
                    is Either.Success -> when (t5) {
                        is Either.Error -> t5
                        is Either.Success ->
                            transform(
                                t1.successVal,
                                t2.successVal,
                                t3.successVal,
                                t4.successVal,
                                t5.successVal
                            )
                        }
                    }
                }
            }
        }
    }

    /**
     * Extension to [combineTransform] that handles Either.Error cases.
     */
    fun <T1, T2, R> combineTransformWithError(
        flow1: Flow<Either<IFailure, T1>>,
        flow2: Flow<Either<IFailure, T2>>,
        transform: suspend FlowCollector<Either<IFailure, R>>.(T1, T2) -> Unit
    ): Flow<Either<IFailure, R>> = combineTransform(flow1, flow2) { t1, t2 ->
        when (t1) {
            is Either.Error -> emit(t1)
            is Either.Success -> when (t2) {
                is Either.Error -> emit(t2)
                is Either.Success -> transform(t1.successVal, t2.successVal)
            }
        }
    }


/**
 * Extension to [combineTransform] that handles Either.Error cases.
 */
fun <T1, T2, T3, R> combineTransformWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    flow3: Flow<Either<IFailure, T3>>,
    transform: suspend FlowCollector<Either<IFailure, R>>.(T1, T2, T3) -> Unit
): Flow<Either<IFailure, R>> = combineTransform(flow1, flow2, flow3) { t1, t2, t3 ->
    when (t1) {
        is Either.Error -> emit(t1)
        is Either.Success -> when (t2) {
            is Either.Error -> emit(t2)
            is Either.Success -> when (t3) {
                is Either.Error -> emit(t3)
                is Either.Success -> transform(t1.successVal, t2.successVal, t3.successVal)
            }
        }
    }
}


/**
 * Extension to [combineTransformAny] that handles [Either.Error] cases
 */
fun <T1, T2, R> combineTransformAnyWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    transform: suspend FlowCollector<Either<IFailure, R>>.(T1?, T2?) -> Unit
): Flow<Either<IFailure, R>> =
    combineTransformAny(flow1, flow2) { t1, t2 ->
        when (t1) {
            is Either.Error -> emit(t1)
            else -> when (t2) {
                is Either.Error -> emit(t2)
                else -> transform(t1?.successValue(), t2?.successValue())
            }

        }
    }


/**
 * Extension to [combineTransformAny] that handles [Either.Error] cases
 */
fun <T1, T2, T3, R> combineTransformAnyWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    flow3: Flow<Either<IFailure, T3>>,
    transform: suspend FlowCollector<Either<IFailure, R>>.(T1?, T2?, T3?) -> Unit
): Flow<Either<IFailure, R>> = combineTransformAny(
    flow1,
    flow2,
    flow3
) { t1, t2, t3 ->
    when (t1) {
        is Either.Error -> emit(t1)
        else -> when (t2) {
            is Either.Error -> emit(t2)
            else -> when (t3) {
                is Either.Error -> emit(t3)
                else -> transform(t1?.successValue(), t2?.successValue(), t3?.successValue())
            }
        }
    }
}


/**
 * Extension to [combineAny] that handles [Either.Error] cases
 */
fun <T1, T2, R> combineAnyWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    transform: suspend (T1?, T2?) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> =
    combineAny(flow1, flow2) { t1, t2 ->
        when (t1) {
            is Either.Error -> t1
            else -> when (t2) {
                is Either.Error -> t2
                else -> transform(t1?.successValue(), t2?.successValue())
            }
        }
    }

/**
 * Extension to [combineAny] that handles [Either.Error] cases
 */
fun <T1, T2, T3, R> combineAnyWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    flow3: Flow<Either<IFailure, T3>>,
    transform: suspend (T1?, T2?, T3?) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> =
    combineAny(flow1, flow2, flow3) { t1, t2, t3 ->
        when (t1) {
            is Either.Error -> t1
            else -> when (t2) {
                is Either.Error -> t2
                else -> when (t3) {
                    is Either.Error -> t3
                    else -> transform(t1?.successValue(), t2?.successValue(), t3?.successValue())
                }
            }
        }
    }

fun <T1, T2, R> withLatestFromWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    transform: suspend (T1, T2) -> Either<IFailure, R>
): Flow<Either<IFailure, R>> = flow1.withLatestFrom(flow2) { t1, t2 ->
    when(t1) {
        is Either.Error -> t1
        is Either.Success -> when(t2) {
            is Either.Error -> t2
            is Either.Success -> transform(t1.successVal, t2.successVal)
        }
    }
}

fun <T1, T2, R> transformWithLatestFromWithError(
    flow1: Flow<Either<IFailure, T1>>,
    flow2: Flow<Either<IFailure, T2>>,
    transform: suspend FlowCollector<Either<IFailure, R>>.(T1, T2) -> Unit
): Flow<Either<IFailure, R>> = flow1.transformWithLatestFrom(flow2) { t1, t2 ->
    when(t1) {
        is Either.Error -> emit(t1)
        is Either.Success -> when(t2) {
            is Either.Error -> emit(t2)
            is Either.Success -> transform(t1.successVal, t2.successVal)
        }
    }
}