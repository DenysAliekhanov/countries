package com.example.countries.domain_base.common

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.cancel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicReference


fun <A, B : Any, R> Flow<A>.withLatestFrom(
    other: Flow<B>,
    transform: suspend (A, B) -> R
): Flow<R> = flow {
    //Source: https://github.com/Kotlin/kotlinx.coroutines/pull/1315#issuecomment-525338743
    coroutineScope {
        val latestB = AtomicReference<B?>()
        val outerScope = this
        launch {
            try {
                other.collect { latestB.set(it) }
            } catch (e: CancellationException) {
                outerScope.cancel(e) // cancel outer scope on cancellation exception, too
            }
        }
        collect { a: A ->
            latestB.get()?.let { b -> emit(transform(a, b)) }
        }
    }
}

fun <A, B : Any, R> Flow<A>.transformWithLatestFrom(
    other: Flow<B>,
    transform: suspend FlowCollector<R>.(A, B) -> Unit
): Flow<R> = flow {
    //Source: https://github.com/Kotlin/kotlinx.coroutines/pull/1315#issuecomment-525338743
    coroutineScope {
        val latestB = AtomicReference<B?>()
        val outerScope = this
        launch {
            try {
                other.collect { latestB.set(it) }
            } catch (e: CancellationException) {
                outerScope.cancel(e) // cancel outer scope on cancellation exception, too
            }
        }
        collect { a: A ->
            latestB.get()?.let { b -> transform(a, b) }
        }
    }
}


/**
 * Like [combine], but the difference is that this operator calls [transform] **as soon as any one flow emits**.
 * It does not wait for both flows to have at least one emission before it starts emitting.
 *
 * For this reason, the parameters to the transform lambda are nullable.
 */
fun <T1, T2, R> combineAny(
    flow1: Flow<T1>,
    flow2: Flow<T2>,
    transform: suspend (T1?, T2?) -> R
): Flow<R> {
    val nullableFlow1 = flow1.map<T1, T1?> { it }.onStart { emit(null) }
    val nullableFlow2 = flow2.map<T2, T2?> { it }.onStart { emit(null) }

    return combineTransform(nullableFlow1, nullableFlow2) { a, b ->
        if (a != null || b != null) emit(transform(a, b))
    }
}

/**
 * Like [combine], but the difference is that this operator calls [transform] **as soon as any one flow emits**.
 * It does not wait for all source flows to have at least one emission before it starts emitting.
 *
 * For this reason, the parameters to the transform lambda are nullable.
 */
fun <T1, T2, T3, R> combineAny(
    flow1: Flow<T1>,
    flow2: Flow<T2>,
    flow3: Flow<T3>,
    transform: suspend (T1?, T2?, T3?) -> R
): Flow<R> {
    val nullableFlow1 = flow1.map<T1, T1?> { it }.onStart { emit(null) }
    val nullableFlow2 = flow2.map<T2, T2?> { it }.onStart { emit(null) }
    val nullableFlow3 = flow3.map<T3, T3?> { it }.onStart { emit(null) }

    return combineTransform(nullableFlow1, nullableFlow2, nullableFlow3) { a, b, c ->
        if (a != null || b != null || c != null) emit(transform(a, b, c))
    }
}

/**
 * Like [combineTransform], but the difference is that this operator calls [transform] **as soon as any one flow emits**.
 * It does not wait for both flows to have at least one emission before it starts emitting.
 *
 * For this reason, the parameters to the transform lambda are nullable.
 */
fun <T1, T2, R> combineTransformAny(
    flow1: Flow<T1>,
    flow2: Flow<T2>,
    transform: suspend FlowCollector<R>.(T1?, T2?) -> Unit
): Flow<R> {
    val nullableFlow1 = flow1.map<T1, T1?> { it }.onStart { emit(null) }
    val nullableFlow2 = flow2.map<T2, T2?> { it }.onStart { emit(null) }

    return combineTransform(nullableFlow1, nullableFlow2) { a, b ->
        if (a != null || b != null) transform(a, b)
    }
}

/**
 * Like [combineTransform], but the difference is that this operator calls [transform] **as soon as any one flow emits**.
 * It does not wait for all source flows to have at least one emission before it starts emitting.
 *
 * For this reason, the parameters to the transform lambda are nullable.
 */
fun <T1, T2, T3, R> combineTransformAny(
    flow1: Flow<T1>,
    flow2: Flow<T2>,
    flow3: Flow<T3>,
    transform: suspend FlowCollector<R>.(T1?, T2?, T3?) -> Unit
): Flow<R> {
    val nullableFlow1 = flow1.map<T1, T1?> { it }.onStart { emit(null) }
    val nullableFlow2 = flow2.map<T2, T2?> { it }.onStart { emit(null) }
    val nullableFlow3 = flow3.map<T3, T3?> { it }.onStart { emit(null) }

    return combineTransform(nullableFlow1, nullableFlow2, nullableFlow3) { a, b, c ->
        if (a != null || b != null || c != null) transform(a, b, c)
    }
}
