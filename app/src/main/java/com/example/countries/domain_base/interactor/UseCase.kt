package com.example.countries.domain_base.interactor

import com.example.countries.domain_base.common.Either
import com.example.countries.domain_base.error.IFailure
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

abstract class UseCase<in Params, out Type> where Type : Any? {

    open val requestDelay: Long = 0

    abstract suspend fun run(params: Params): Either<IFailure, Type>

    open operator fun invoke(
        scope: CoroutineScope,
        params: Params,
        onSuccess: (Type) -> Unit = {},
        onFailure: (IFailure) -> Unit = {}
    ) {
        val job = scope.async { run(params) }
        scope.launch {
            delay(requestDelay)
            job.await().either(onFailure, onSuccess)
        }
    }
}