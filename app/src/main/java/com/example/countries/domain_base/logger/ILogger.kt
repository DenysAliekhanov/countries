package com.example.countries.domain_base.logger

interface ILogger {
    fun v(tag: String, message: String, vararg args: Any?)
    fun d(tag: String, message: String, vararg args: Any?)
    fun i(tag: String, message: String, vararg args: Any?)
    fun w(tag: String, message: String, vararg args: Any?)
    fun e(tag: String, t: Throwable? = null, message: String, vararg args: Any?)
}